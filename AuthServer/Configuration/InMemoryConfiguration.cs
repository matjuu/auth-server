﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace AuthServer.Configuration
{
    public class InMemoryConfiguration
    {
        public static IEnumerable<ApiResource> ApiResources => new[]
        {
            new ApiResource("demoresource", "Demo Resource"), 
        };

        public static IEnumerable<Client> ApiClients => new[]
        {
            new Client
            {
                ClientId = "DemoClient",
                ClientSecrets = new []{new Secret{Value = "secret".Sha256()} },
                AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                AllowedScopes = new []{ "demoresource" }
            }
        };

        public static IEnumerable<TestUser> ApiTestUsers => new[]
        {
            new TestUser
            {
                SubjectId = "1",
                Username = "DemoUser",
                Password = "demo"
            }
        };
    }
}
